package tgrouter

const (
	treeTypeMessage = iota
	treeTypeCommand
	treeTypeCallbackQuery
	treeTypeInlineQuery
	treeTypeChosenInlineResult
	treeTypeChatMessage
	treeTypeChatCommand
)

type TreeOption struct {
	step     int
	handlers HandlersChain
	threads  int
	user     bool
	queue    chan *Context
}

type Tree map[int]map[string][]TreeOption
